package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func responseSize(url string, channel chan map[string]int) {
	fmt.Println("Getting", url)
	response, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}
	m := make(map[string]int)
	m[url] = len(body)
	channel <- m
}
func main() {
	urls := []string{"https://vk.com/", "https://golang.org/", "https://golang.org/doc"}
	size := make(chan map[string]int)

	for _, url := range urls {
		go responseSize(url, size)
	}

	for i := 0; i < len(urls); i++ {
		for key, value := range <-size {
			fmt.Println("Key:", key, "Value:", value)
		}
	}

}
