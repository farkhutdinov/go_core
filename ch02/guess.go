package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {

	second := time.Now().Unix()
	fmt.Println(second)
	rand.Seed(second)
	target := rand.Intn(100) + 1

	fmt.Printf("%d I've chosen a random number between 1 and 100.", target)
	fmt.Println("Can you guess it?")

	success := false
	for guesses := 0; guesses < 10; guesses++ {

		fmt.Print("Enter a number: ")
		reader := bufio.NewReader(os.Stdin)
		input, err := reader.ReadString('\n')

		if err != nil {
			log.Fatal(err)
		}

		input = strings.TrimSpace(input)
		answer, err := strconv.Atoi(input)

		if err != nil {
			log.Fatal(err)
		}

		if answer == target {
			success = true
			fmt.Println("Goood!!!")
			break
		} else {
			fmt.Println("Badd")
		}
	}

	if !success {
		fmt.Println("You loser")
	}
}
