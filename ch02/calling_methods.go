package main

import (
	"fmt"
	"strings"
	"time"
)

func main() {
	var now = time.Now()
	var year int = now.Year()
	fmt.Println(year)
	fmt.Println(now.UTC())

	broken := "G# r#cks!"
	replacer := strings.NewReplacer("#", "o")
	fixed := replacer.Replace(broken)
	fmt.Println(fixed)
}
