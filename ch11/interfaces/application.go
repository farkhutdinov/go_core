package main

import (
	"fmt"
	"gitlab.com/farkhutdinov/go_core/ch11/interfaces/gadget"
)

func playList(device gadget.Player, songs []string) {
	for _, song := range songs {
		device.Play(song)
	}
	device.Stop()
	tapeRecorder, ok := device.(gadget.TapeRecorder)
	if ok {
		tapeRecorder.Record()
	} else {
		fmt.Println("Player was not a TapeRecorder")
	}
	fmt.Println("___________________________________")
}

func main() {
	mixtape := []string{"Jessie's Girl", "Whip It", "9 to 5"}

	tapePlayer := gadget.TapePlayer{}
	tapePlayer.Batteries = "WWW-220"

	var player gadget.Player = &tapePlayer

	playList(player, mixtape)

	player = gadget.TapeRecorder{}
	playList(player, mixtape)

}
