package gadget

import "fmt"

type TapePlayer struct {
	Batteries string
}

func (t *TapePlayer) Play(song string) {
	if t.Batteries == "WWW-220" {
		fmt.Println("Wow-wow!!!")
	}
	fmt.Println("Playing on TapePlayer ", song)
}
func (t TapePlayer) Stop() {
	fmt.Println("Stopped TapePlayer!")
}

type TapeRecorder struct {
	Microphones int
}

func (t TapeRecorder) Play(song string) {
	fmt.Println("Playing on TapeRecorder ", song)
}
func (t TapeRecorder) Record() {
	fmt.Println("Recording TapeRecorder ")
}
func (t TapeRecorder) Stop() {
	fmt.Println("Stopped TapeRecorder!")
}
