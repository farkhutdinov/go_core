package gadget

type Player interface {
	Play(string)
	Stop()
}
