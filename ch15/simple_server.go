package main

import (
	"fmt"
	"log"
	"net/http"
)

func viewHandler(response http.ResponseWriter, r *http.Request) {
	message := "Hello World"
	fmt.Print(message)
	fmt.Fprintf(response, "Hello Go!")
}

func main() {
	http.HandleFunc("/hello", viewHandler)
	err := http.ListenAndServe("localhost:8080", nil)
	log.Fatal(err)
}
