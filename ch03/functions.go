package main

import "fmt"

func main() {
	myAge := 24
	returnMyAge(&myAge)
	changeAndReturnMyAge(&myAge)
	fmt.Println("myAge=", myAge)

	truth := true
	negate(&truth)
	fmt.Println(truth)
	lies := false
	negate(&lies)
	fmt.Println(lies)
}

func returnMyAge(age *int) {
	fmt.Println("age from returnMyAge=", *age)
}

func changeAndReturnMyAge(age *int) {
	*age = 25
	fmt.Println("age from changeAndReturnMyAge=", *age)
}
func negate(myBoolean *bool) {
	*myBoolean = !*myBoolean
}
