package main

import "fmt"

type Liters float64
type Gallons float64

func main() {
	carFuel := Gallons(10.0)
	busFuel := Liters(240.0)
	fmt.Println(carFuel, busFuel)
	carFuel = Gallons(busFuel * 0.264)
	busFuel = Liters(carFuel * 3.785)
	fmt.Printf("Gallons: %0.1f\nLiters: %0.1f\n", carFuel, busFuel)
}
