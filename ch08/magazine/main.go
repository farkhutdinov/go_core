package main

import (
	"fmt"
	"gitlab.com/farkhutdinov/go_core/ch08/magazine/data"
)

func main() {
	addr_1 := data.Address{Street: "St.Green", City: "New York", State: "New York", PostalCode: "2323211"}
	subscribe := data.Subscribe{Name: "John Smith", Rate: 666.6, Active: true, Address: addr_1}
	fmt.Printf("[Subscribe:{\n"+
		"\t\t\tName=%s,\n"+
		"\t\t\tRate=%.1f,\n"+
		"\t\t\tActive=%t,\n"+
		"\t\t\tAddress {\n"+
		"\t\t\t\t\tStreet=%s\n"+
		"\t\t\t\t\tCity =%s\n"+
		"\t\t\t\t\tState =%s\n"+
		"\t\t\t\t\tPostalCode =%s\n\t\t\t\t\t}\n}",

		subscribe.Name, subscribe.Rate, subscribe.Active, subscribe.Street, subscribe.City,
		subscribe.State, subscribe.PostalCode)
}
