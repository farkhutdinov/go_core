package main

import (
	"github.com/headfirstgo/greeting"
	"github.com/headfirstgo/greeting/dansk"
	"github.com/headfirstgo/greeting/deutsch"
)

func main() {
	deutsch.Hallo()
	dansk.GodMorgen()
	greeting.Hello()
}
