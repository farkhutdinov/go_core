package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(maximum(12.4, 123.3, 342.1, 93.1))
}

func maximum(arguments ...float64) float64 {
	max := math.Inf(-1)

	for _, number := range arguments {
		if number > max {
			max = number
		}
	}
	return max
}
