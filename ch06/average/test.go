package main

import (
	"fmt"
	"gitlab.com/farkhutdinov/go_core/ch06/getnumbers"
	"log"
)

func main() {
	numbers, err := getnumbers.GetFloats("ch06/data.txt")
	if err != nil {
		log.Fatal(err)
	}
	var sum float64 = 0
	for _, number := range numbers {
		sum += number
	}
	sampleCount := float64(len(numbers))
	fmt.Printf("Average: %0.2f\n", sum/sampleCount)
}
