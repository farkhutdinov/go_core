package main

import (
	"fmt"
	"gitlab.com/farkhutdinov/go_core/ch05/datafile"
	"log"
)

func main() {
	numbers, err := datafile.GetFloats("ch05/data.txt")
	if err != nil {
		log.Fatal(err)
	}

	var sum float64 = 0

	for _, element := range numbers {
		sum += element
	}

	sampleCount := sum / float64(len(numbers))
	fmt.Printf("Average: %0.2f\n", sum/sampleCount)
}
