package main

import (
	"fmt"
	"gitlab.com/farkhutdinov/go_core/ch07/datafile"
	"log"
	"time"
)

func main() {
	start := time.Now()
	lines, err := datafile.GetStrings("ch07/count/votes.txt")
	if err != nil {
		log.Fatal(err)
	}
	counts := make(map[string]int64)
	for _, line := range lines {
		counts[line]++
	}
	for name, count := range counts {
		fmt.Printf("Votes for %s: %d\n", name, count)
	}
	_, ok := counts["Вова Путин"]
	fmt.Println(ok)
	duration := time.Since(start)
	fmt.Println(duration)
}
